package dummydomain.yetanothercallblocker.sia.network;

import okhttp3.OkHttpClient;

public interface OkHttpClientFactory {

    OkHttpClient getOkHttpClient();

}
