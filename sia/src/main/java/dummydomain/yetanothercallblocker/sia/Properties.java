package dummydomain.yetanothercallblocker.sia;

public interface Properties {

    int getInt(String key, int defValue);

    void setInt(String key, int value);

}
